## Welcome to PhotoRetriever! 

*(no comments on (lack of) naming originality, please)*

--- 

## Source 


https://bitbucket.org/sccornboy/photoretriever/src/master/

---

## Summary

PhotoRetriever was developed as a 2-piece solution to retrieve photo information from a 3rd party resource: 

1. **PhotoRetrieverApi** - Web api with the responsibility of actual photo information retrieval from 3rd party resource.  This was built with domain driven design in mind, but it its internal workings are solely focused on getting photo information for an album based on its ID (i.e., single domain, models, repositories, etc.).  

2. **PhotoRetrieverConsoleApp** - Console application to dispaly photo information from the 3rd party resource.  Like the PhotoRetrieverApi, this is highly focused on getting photo information for an album based on its ID.  Additionally, due to time concerns and constraints, this application purposely lacks some of the usual design considerations implemented in the PhotoRetrieverApi, such domain-driven design, inversion of control, and unit tests.


---

## Build and Run

-  Ensure **.Net Framework 4.6.1** is installed 
-  Open solution the **PhotoRetriever.sln** using **VisualStudio 2017**


####OPTION #1 - The Easiest Way to See Things In Action

1.  Set solution to have **multiple startup projects**:

	a.  In the **VS 2017 Solution Explorer**, right-click on the solution	
	b.  Select **Set StartUp Projects...**	
	c.  Select **Multiple startup projects**	
	
	*      PhotoRetrieverApi --> **Action = Start**
	*      PhotoRetrieverConsoleApp --> **Action = Start**

2.  Initialization PhotoRetireverConsoleApp

	a.  In the VS 2017 Solution Explorer, right-click on the **PhotoRetrieverConsoleApp** project and select **Debug --> Start New Instance**	
	b.  If a Microsoft Visual Studio dialogue pops up regarding the security debugging option, then hit **OK**	
	c.  Stop the debugger (i.e., close the console application)

3.  In the **VS 2017 Standard Toolbar**, click **Start**


###OPTION #2

1.  Publish PhotoRetrieverConsoleApp

	a.  Right-click on the **PhotoRetrieverConsoleApp** project and select **Publish**	
	b.  Input a publication destination and click **Next**
    
	- *ex:   C:\projects\publish\PhotoRetrieverConsoleApp*
	
	c.  Select the option for **From a CD-ROM or DVD-ROM** and click **Next**	
	d.  Select the option for **The application will not check for updates** and click **Next**	
	e.  Click **Finish**

2.  Deploy the console app

	a.	Once publication completes, run the **setup.exe** in the publication destination folder

	b.	Click **Install** *(shortcut should exist in the **Start Menu** and application should automatically open)*

3.  Debug PhotoRetrieverApi

	a.  In the VS 2017 Solution Explorer, right-click on the **PhotoRetrieverApi** project and select **Debug --> Start new instance**


---

## Usage (from VS 2017)

1.  In the **PhotoRetrieverApi**, ensure the following shows:  **'Application Started.  Press Ctrl+C to shut down.'**

2.  In the **Photo Retriever Console App**, enter **'photo-album '** followed by by a **photo album ID** and hit ENTER
    
	- *ex:   photo-album 3*

---

##Possible Issues

+ Nuget package version mismatch (*PhotoRetrieverApi*)

	Some nuget packages required older versions. If errors arise while building, then verify the following package have the appropriate versions:

	+ Microsoft.Extensions.Configuration -->  **version 2.1.1**
	+ Microsoft.Extensions.Configuration.Binder -->  **version 2.1.1**
	+ Microsoft.Extensions.DependencyInjection.Abstractions -->  **version 1.1.0**
	+ Microsoft.Extensions.Logging -->  **version 2.1.1**
	
	

+ Certificate Issues (*PhotoRetrieverApi*)

	A dialog may come up on first VS 2017 debug saying certificate missing and asking if you want to use/trust the one it can create. Go ahead and accept it.
	
	
	
---
