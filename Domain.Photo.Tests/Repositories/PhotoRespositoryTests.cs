namespace Domain.Photo.Tests
{
    using System.Collections.Generic;
    using Domain.Photo;
    using Infrastructure;
    using Moq;
    using Xunit;

    public class PhotoRepositoryTests
    {
        [Fact]
        public async void When_IdIsValid_Expect_ListOfPhotos()
        {
            // Arrange
            var photoSource = new Mock<IPhotoSource>();

            var photoList = new List<Core.Photo>
            {
                new Core.Photo { AlbumId = 1, Id = 101, Title = "Some title" }
            };

            photoSource.Setup(c => c.GetByAlbumId(It.IsAny<int>())).ReturnsAsync(photoList);

            var photoRepository = new PhotoRepository(photoSource.Object);

            // Act
            var result = await photoRepository.GetByAlbumId(1);

            // Assert
            Assert.IsType<List<Core.Photo>>(result);
            Assert.NotNull(result);
        }
    }
}
