﻿namespace PhotoRetrieverApi
{
    using System;
    using Autofac;
    using Autofac.Extensions.DependencyInjection;
    using AutoMapper;
    using log4net;
    using Microsoft.AspNetCore.Builder;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.Extensions.Logging;

    public class Startup
    {

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;            
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Latest);
            services.AddSingleton(Configuration);
            
            services.AddHttpClient();
            services.AddAutoMapper();

            return BuildAutofacContainer(services);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            loggerFactory.AddLog4Net();

            app.UseHttpsRedirection();
            app.UseMvc();
        }

        public IServiceProvider BuildAutofacContainer(IServiceCollection services)
        {
            var containerBuilder = new ContainerBuilder();

            containerBuilder.Register(c => LogManager.GetLogger(typeof(object))).As<ILog>();

            containerBuilder.RegisterModule<Core.AutofacModule>();
            containerBuilder.RegisterModule<Infrastructure.AutofacModule>();
            containerBuilder.RegisterModule<Domain.Photo.AutofacModule>();

            containerBuilder.Populate(services);

            return new AutofacServiceProvider(containerBuilder.Build());
        }
    }
}
