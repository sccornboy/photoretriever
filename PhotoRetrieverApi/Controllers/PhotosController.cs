﻿namespace PhotoRetrieverApi.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Core;
    using Domain.Photo;
    using log4net;
    using Microsoft.AspNetCore.Mvc;
    using Newtonsoft.Json;

    [Route("api/[controller]")]
    [ApiController]
    public class PhotosController : ControllerBase
    {
        private static IPhotoRepository _photoRepository;
        private static ILog _log;

        public PhotosController(IPhotoRepository photoRepository, ILog log)
        {
            _photoRepository = photoRepository;
            _log = log;
        }

        // GET api/photos/album/5
        [HttpGet("album/{id}")]
        public async Task<IActionResult> GetByAlbumId(int id)
        {
            try
            {
                var photos = await _photoRepository.GetByAlbumId(id);
                var result = new Results()
                {
                    result = "SUCCESS",
                    data = photos
                };

                return Ok(JsonConvert.SerializeObject(photos));
            }
            catch(Exception ex)
            {
                var message = string .Format("Error occured trying to retrieve photos for albumId = {0}!", id);
                _log.ErrorFormat("{0}{1}{2}", message, Environment.NewLine, ex.ToString());
                return BadRequest(message);
            }
        }
    }

    public class Results
    {
        public string result { get; set; }
        public List<Photo> data { get; set; }
    }
}
