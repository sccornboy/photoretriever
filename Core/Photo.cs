﻿namespace Core
{
    public class Photo : IPhoto
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public int AlbumId { get; set; }
    }
}
