﻿namespace Core
{
    using Autofac;

    public class AutofacModule : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<Photo>().As<IPhoto>();
        }
    }
}