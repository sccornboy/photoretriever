﻿namespace Infrastructure
{
    using AutoMapper;

    public class Automapper : Profile
    {
        public Automapper()
        {
            Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<WebRef.Models.Photo, Core.Photo>()
                    .ForMember(dest => dest.AlbumId, src => src.MapFrom(opt => opt.albumId))
                    .ForMember(dest => dest.Id, src => src.MapFrom(opt => opt.id));
            });

            Mapper.AssertConfigurationIsValid();
        }
    }
}