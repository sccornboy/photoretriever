﻿namespace Infrastructure
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Core;
    
    public interface IPhotoSource
    {
        Task<List<Photo>> GetByAlbumId(int id);
    }
}
