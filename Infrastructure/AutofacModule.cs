﻿namespace Infrastructure
{
    using Autofac;

    public class AutofacModule : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<PhotoSource1>().As<IPhotoSource>();
            builder.RegisterType<ApiClient>().As<IApiClient>();
        }
    }
}