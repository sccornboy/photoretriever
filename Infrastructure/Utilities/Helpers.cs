﻿namespace Infrastructure.Utility
{
    using System;

    public class Helpers
    {
        public static bool ValidateUrl(string url, out string message)
        {          
            if (string.IsNullOrWhiteSpace(url))
            {
                message = "Url is empty!";
                return false;
            }

            if (!Uri.TryCreate(url, UriKind.Absolute, out Uri uri) || (uri.Scheme != Uri.UriSchemeHttp && uri.Scheme != Uri.UriSchemeHttps))
            {
                message = string.Format("Url format invalid: {0}", url);
                return false;
            }

            message = string.Empty;
            return true;
        }
    }
}
