﻿namespace Infrastructure
{
    using System;
    using System.Net.Http;
    using System.Net.Http.Headers;
    using System.Threading.Tasks;
    using Infrastructure.Utility;
    using Newtonsoft.Json;

    public class ApiClient : IApiClient
    {
        private static IHttpClientFactory _httpClientFactory;

        public ApiClient(IHttpClientFactory httpClientFactory)
        {
            _httpClientFactory = httpClientFactory;
        }

        public async Task<T> GetRequest<T>(string url)
        {
            var client = _httpClientFactory.CreateClient();

            if(!Helpers.ValidateUrl(url, out string message))
            {
                throw new Exception(message);
            }

            var uri = new Uri(url);

            client.BaseAddress = new Uri(string.Format("{0}://{1}", uri.Scheme, uri.Authority)); ; 
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            var response = await client.GetAsync(uri.PathAndQuery);
            response.EnsureSuccessStatusCode();

            var content = await response.Content.ReadAsStringAsync();       

            return JsonConvert.DeserializeObject<T>(content);
        }
    }
}
