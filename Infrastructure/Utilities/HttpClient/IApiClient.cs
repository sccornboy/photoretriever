﻿namespace Infrastructure
{
    using System.Collections.Generic;
    using System.Threading.Tasks;

    public interface IApiClient
    {
        Task<T> GetRequest<T>(string url);
    }
}
