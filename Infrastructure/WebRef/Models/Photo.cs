﻿namespace Infrastructure.WebRef.Models
{
    using AutoMapper.Configuration.Conventions;

    public class Photo
    {
        public int albumId { get; set; }

        public int id { get; set; }

        public string title { get; set; }

        public string url { get; set; }

        public string thumbnailUrl { get; set; }
    }
}
