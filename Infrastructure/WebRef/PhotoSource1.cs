﻿namespace Infrastructure
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using AutoMapper;
    using Infrastructure.Utility;
    using Infrastructure.WebRef.Models;
    using Microsoft.Extensions.Configuration;

    public class PhotoSource1 : IPhotoSource
    {
        private static IConfiguration _configuration;
        private static IApiClient _apiClient;
        private static IMapper _mapper;
        private static string _url;

        public PhotoSource1(IConfiguration configuration, IApiClient apiClient, IMapper mapper)
        {
            _configuration = configuration;
            _apiClient = apiClient;
            _mapper = mapper;
        }

        public async Task<List<Core.Photo>> GetByAlbumId(int id)
        {
            var url = string.Format("{0}?albumId={1}", _configuration.GetSection("Urls").GetSection("PhotoSource1").Value, id);

            if (!Helpers.ValidateUrl(url, out string message))
            {
                throw new Exception(message);
            }

            _url = url;
            var photos = await _apiClient.GetRequest<List<Photo>>(_url);
            var corePhotos = _mapper.Map<List<Core.Photo>>(photos);
            return corePhotos;
        }        
    }
}