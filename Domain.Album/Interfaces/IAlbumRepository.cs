﻿namespace Domain.Album
{
    using System.Collections.Generic;
    using Core;

    public interface IAlbumRepository
    {
        IEnumerable<Album> GetAll();

        Album GetById(int id);
    }
}
