﻿namespace Domain.Album
{
    using System.Collections.Generic;
    using Core;

    public interface IPhotoRepository
    {
        IEnumerable<Photo> GetByAlbumId(int id);
    }
}
