﻿namespace Domain.Album
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Core;
    using Infrastructure;

    public class AlbumRepository : IAlbumRepository
    {
        private static Infrastructure.IAlbum _album;

        public AlbumRepository(Infrastructure.IAlbum album)
        {
            _album = album;
        }

        public IEnumerable<Core.Album> GetAll()
        {
            var albums =  _album.GetAll();
            return albums;
        }

        public Core.Album GetById(int id)
        {
            var album = _album.GetById(id);
            return album;
        }
    }
}
