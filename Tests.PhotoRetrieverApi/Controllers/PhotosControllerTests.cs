namespace PhotoRetrieverApi.Tests
{
    using System;
    using System.Collections.Generic;
    using Domain.Photo;
    using log4net;
    using Microsoft.AspNetCore.Mvc;
    using Moq;
    using PhotoRetrieverApi.Controllers;
    using Xunit;

    public class PhotosControllerTests
    {      
        [Fact]
        public async void When_IdIsValid_Expect_OkResult()
        {
            // Arrange
            var photoRepository = new Mock<IPhotoRepository>();
            var log = new Mock<ILog>();

            var photoList = new List<Core.Photo>
            {
                new Core.Photo { AlbumId = 1, Id = 101, Title = "Some title" }
            };

            photoRepository.Setup(c => c.GetByAlbumId(It.IsAny<int>())).ReturnsAsync(photoList);

            var photoController = new PhotosController(photoRepository.Object, log.Object);

            // Act
            var result = await photoController.GetByAlbumId(1);
            var okResult = result as OkObjectResult;

            // Assert
            Assert.NotNull(okResult);
            Assert.Equal(200, okResult.StatusCode);
        }

        [Fact]
        public async void When_IdNotValid_Expect_BadRequestResult()
        {
            // Arrange
            var photoRepository = new Mock<IPhotoRepository>();
            var log = new Mock<ILog>();

            photoRepository.Setup(c => c.GetByAlbumId(It.IsAny<int>())).ThrowsAsync(new Exception());

            var photoController = new PhotosController(photoRepository.Object, log.Object);

            // Act
            var result = await photoController.GetByAlbumId(-1);
            var badRequestResult = result as BadRequestObjectResult;

            // assert
            Assert.NotNull(badRequestResult);
            Assert.Equal(400, badRequestResult.StatusCode);
        }
    }
}
