﻿namespace Infrastructure.Tests
{
    using System;
    using System.Collections.Generic;
    using AutoMapper;
    using Infrastructure;
    using Infrastructure.WebRef.Models;
    using Microsoft.Extensions.Configuration;
    using Moq;
    using Xunit;

    public class PhotoSource1Tests
    {
        [Fact]
        public async void When_UrlInvalid_Expect_Exception()
        {
            // Arrange
            var configuration = new Mock<IConfiguration>();
            var apiClient = new Mock<IApiClient>();
            var mapper = new Mock<IMapper>();

            var url = "//somebadurl";
            configuration.Setup(c => c.GetSection("Urls").GetSection("PhotoSource1").Value).Returns(url);

            var photoSource = new PhotoSource1(configuration.Object, apiClient.Object, mapper.Object);

            // Act
            var exception = await Assert.ThrowsAnyAsync<Exception>(() => photoSource.GetByAlbumId(1));

            // Assert
            Assert.NotNull(exception);
            Assert.False(string.IsNullOrWhiteSpace(exception.Message));
        }

        [Fact]
        public async void When_IdIsValid_Expect_ListOfCorePhotos()
        {
            // Arrange
            var configuration = new Mock<IConfiguration>();
            var apiClient = new Mock<IApiClient>();
            var mapper = new Mock<IMapper>();

            var photoList = new List<Photo>()
            {
                new Photo { albumId = 1, id = 101, title = "Some title", url = "http://some.url.com", thumbnailUrl = "http://some.thumbnailurl.com" }
            };

            var corePhotoList = new List<Core.Photo>()
            {
                new Core.Photo { AlbumId = 1, Id = 101, Title = "Some title" }
            };

            var url = "http://someplace.somewhere.com/photos";
            apiClient.Setup(c => c.GetRequest<List<Photo>>(It.IsAny<string>())).ReturnsAsync(photoList);
            configuration.Setup(c => c.GetSection("Urls").GetSection("PhotoSource1").Value).Returns(url);
            mapper.Setup(c => c.Map<List<Core.Photo>>(It.IsAny<List<Photo>>())).Returns(corePhotoList);

            var photoSource = new PhotoSource1(configuration.Object, apiClient.Object, mapper.Object);

            // Act
            var result = await photoSource.GetByAlbumId(1);

            // Assert
            Assert.IsType<List<Core.Photo>>(result);
            Assert.NotNull(result);
        }
    }
}
