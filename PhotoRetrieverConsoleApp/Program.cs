﻿namespace PhotoRetrieverConsoleApp
{
    using System;
    using System.Threading.Tasks;

    class Program
    {    
        public static void Main(string[] args)
        {
            try
            {
                // work around to handle async calls
                MainAsync(args).GetAwaiter().GetResult();
            }
            catch (Exception ex)
            {
                // Log exceptions
                Console.WriteLine(ex.Message);
            }
            finally
            {
                Console.WriteLine("");
            }

        }

        private static async Task MainAsync(string[] args)
        {
            var console = new PhotoConsole();
            await console.Start();
        }
    }
}
