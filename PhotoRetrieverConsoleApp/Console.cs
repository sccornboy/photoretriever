﻿namespace PhotoRetrieverConsoleApp
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Linq;
    using System.Net.Http;
    using System.Runtime.InteropServices;
    using System.Threading.Tasks;
    using Newtonsoft.Json;
    using PhotoRetrieverConsoleApp.Models;

    public class PhotoConsole
    {
        private const string _exitCommand = "exit";

        public PhotoConsole() { }

        public async Task Start()
        {
            Console.Title = "Photo Retriever Console App";

            while (true)
            {
                try
                {
                    DisplayInstructions();

                    string line = Console.ReadLine().Trim();
                    if (string.Compare(line, _exitCommand, StringComparison.OrdinalIgnoreCase) == 0)
                    {
                        break;
                    }

                    await ProcessUserInput(line.Split(' '));
                }
                catch (Exception ex)
                {
                    // Log exceptions
                    Console.WriteLine(ex.Message);
                }
                finally
                {
                    Console.WriteLine("");
                }
            }
        }

        private void DisplayInstructions()
        {
            Console.WriteLine("Type 'exit' or hit 'Ctrl^C' to close the application");
            Console.Write("Enter 'Selector' and 'Id': ");
        }

        private async Task ProcessUserInput(string[] args)
        {
            ValidateUserInput(args);
            var photos = await RetrieveData(args);
            OutputPhotos(photos);
        }

        private async Task<List<Photo>> RetrieveData(string[] args)
        {
            if (string.Compare(args[0], InputCommands.ALBUM, true) == 0)
            {
                if (!int.TryParse(args[1], out int albumId))
                {
                    throw new Exception(" --> ERROR: Provided photo album 'Id' is not a number");
                }

                return await GetAlbumPhotos(albumId);
            }
            else
            {
                throw new Exception(" --> ERROR: Provided 'Selector' not recognized");
            }
        }

        private async Task<List<Photo>> GetAlbumPhotos(int albumId)
        {
            var httpClient = InitializeHttpClient();
            
            var response = await httpClient.GetAsync(string.Format("api/photos/album/{0}", albumId));
            var responseContent = await response.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<List<Photo>>(responseContent, new JsonSerializerSettings
                    {
                        NullValueHandling = NullValueHandling.Ignore
                    });
        }

        private HttpClient InitializeHttpClient()
        {
            HttpClient client = new HttpClient
            {
                BaseAddress = new Uri(ConfigurationManager.AppSettings["apiSource1"])
            };

            client.DefaultRequestHeaders.Clear();
            client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("text/plain"));

            return client;
        }

        private void ValidateUserInput(string[] args)
        {
            if (args == null || args.Length == 0)
            {
                throw new ArgumentNullException("args", " --> ERROR: No input provided!");
            }

            if (args.Length == 1)
            {
                throw new ArgumentNullException("args", " --> ERROR: Album Id not provided!");
            }

            if (args.Length > 2)
            {
                throw new ArgumentOutOfRangeException("args", " --> ERROR: Too many arguments provided!");
            }

            if (string.IsNullOrWhiteSpace(args[0]))
            {
                throw new ArgumentNullException("args", " --> ERROR: Provided 'Selector' is null!");
            }

            if (string.IsNullOrWhiteSpace(args[1]))
            {
                throw new ArgumentNullException("args", " --> ERROR: Provided 'Id' is null");
            }
        }

        private void OutputPhotos(List<Photo> photos)
        {
            if(photos == null || photos.Count() == 0)
            {
                Console.Write("No photos found!");
                return;
            }

            Console.WriteLine(string.Join("", photos.Select(p => string.Format("   {0} - {1}\n", p.Id.ToString(), p.Title)).ToArray()));
        }
    }
}
