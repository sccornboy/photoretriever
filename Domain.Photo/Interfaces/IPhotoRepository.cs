﻿namespace Domain.Photo
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Core;
    
    public interface IPhotoRepository
    {
        Task<List<Photo>> GetByAlbumId(int id);
    }
}
