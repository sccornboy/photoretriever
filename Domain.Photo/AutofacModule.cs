﻿namespace Domain.Photo
{
    using Autofac;

    public class AutofacModule : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<PhotoRepository>().As<IPhotoRepository>();
        }
    }
}