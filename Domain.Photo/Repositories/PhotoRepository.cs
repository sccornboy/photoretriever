﻿namespace Domain.Photo
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Core;
    using Infrastructure;

    public class PhotoRepository : IPhotoRepository
    {
        private static IPhotoSource _photoSource;

        public PhotoRepository(IPhotoSource photoSource)
        {
            _photoSource = photoSource;
        }

        public async Task<List<Photo>> GetByAlbumId(int id)
        {
            var photos = await _photoSource.GetByAlbumId(id);
            return photos;
        }
    }
}
